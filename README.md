有兴趣请关注主项目：JeeWeb(https://gitee.com/dataact/jeeweb)
<br />
jeeweb-bbs
-----------------------------------

* 	QQ交流群： 570062301（满）、522959928
* 	官方网站： [https://www.jeeweb.cn](https://www.jeeweb.cn)
* 	项目演示： [https://www.jeeweb.cn](https://www.jeeweb.cn)

文档后续补上
------------
	
启动方法
-----------------------------------
    1.下载代码
    2.导入mysql.sql的SQL
    3.运行 BbsBootApplication
    4.访问 localhost:8081
    5.登录 admin/123456

界面演示
----------------------------------------------------------------------------------
**主界面**
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/120757_9b7d7707_1394985.png "屏幕截图.png")
<br />
**如何交流、反馈、参与贡献？** 
- Git仓库：https://gitee.com/renrenio/renren-generator
- [JeeWeb](https://www.jeeweb.cn)：https://www.jeeweb.cn
- 官方QQ群：570062301（满）、522959928
- 技术讨论、二次开发等咨询、问题和建议，请移步到JeeWeb开源社区，我们将会一一的解答和回复
- 如果感兴趣请Watch、Star项目，同时也是对项目最好的支持